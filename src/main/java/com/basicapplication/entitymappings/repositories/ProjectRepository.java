/**
 * 
 */
package com.basicapplication.entitymappings.repositories;

import org.springframework.data.repository.CrudRepository;

import com.basicapplication.entitymappings.model.Project;

/**
 * @author Needa-Con
 *
 */
public interface ProjectRepository extends CrudRepository<Project, Integer> {

}
