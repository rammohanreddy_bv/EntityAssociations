/**
 * 
 */
package com.basicapplication.entitymappings.repositories;

import org.springframework.data.repository.CrudRepository;

import com.basicapplication.entitymappings.model.Employee;

/**
 * @author Ram
 *
 */
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
