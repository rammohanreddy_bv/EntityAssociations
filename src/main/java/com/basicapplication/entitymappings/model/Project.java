/**
 * 
 */
package com.basicapplication.entitymappings.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



/**
 * @author Ram
 *
 */
@Entity
@Table(name="project")
public class Project {
	 
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String projectName;
	
	private String projectTechnology;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "employee_project", joinColumns = { @JoinColumn(name = "PROJECT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "EMPLOYEE_ID") })
	private Set<Employee> employeeList = new HashSet<Employee>();

	
	public Project() {
		
	}
	
	public Project(int id, String projectName, String projectTechnology, Set<Employee> employeeList) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.projectTechnology = projectTechnology;
		this.employeeList = employeeList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectTechnology() {
		return projectTechnology;
	}

	public void setProjectTechnology(String projectTechnology) {
		this.projectTechnology = projectTechnology;
	}

	public Set<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(Set<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", projectName=" + projectName + ", projectTechnology=" + projectTechnology
				+ ", employeeList=" + employeeList + "]";
	}
	
	

}
