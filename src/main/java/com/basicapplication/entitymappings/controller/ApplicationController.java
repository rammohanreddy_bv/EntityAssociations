/**
 * 
 */
package com.basicapplication.entitymappings.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.basicapplication.entitymappings.dto.AssignProjectDTO;
import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.dto.JoinEmployeeDTO;
import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.service.EmployeeService;
import com.basicapplication.entitymappings.service.ProjectService;

/**
 * @author Needa-Con
 *
 */

@RestController
public class ApplicationController {
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	ProjectService projectService;
	
	@PostMapping("/join")
	public List<ProjectDTO> join(@RequestBody AssignProjectDTO assignProjectDTO) {
		// one person id, multiple teamIds
		return employeeService.assignProjects(assignProjectDTO);
	}
	
	@PostMapping("/assign")
	public List<EmployeeDTO> assign(@RequestBody JoinEmployeeDTO joinEmployeeDTO) {
		// one team id, multiple persons
		return projectService.joinEmployees(joinEmployeeDTO);
	}
	
	


}
