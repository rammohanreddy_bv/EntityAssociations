/**
 * 
 */
package com.basicapplication.entitymappings.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.model.Employee;
import com.basicapplication.entitymappings.service.EmployeeService;

/**
 * @author Ram
 *
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	
	@Autowired
	EmployeeService employeeService;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Employee create(@RequestBody EmployeeDTO employeeDTO) {

		System.out.println("Rammohan");
		return employeeService.create(employeeDTO);
	}
	
	
	@GetMapping
	public @ResponseBody List<EmployeeDTO> getAllEmployees() {
		return employeeService.getAllEmployees();
		
		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody EmployeeDTO getEmployee(@PathVariable("id") int id) {
		
		return employeeService.getEmployee(id);
	}
	
	
	@PutMapping(value="/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody EmployeeDTO updateEmployee(@RequestBody EmployeeDTO employeeDTO,@PathVariable("id") int id) {
		
		return employeeService.updateEmployee(employeeDTO,id);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteEmployee(@PathVariable("id") int id) {
		employeeService.deleteEmployee(id);
	}

}
