/**
 * 
 */
package com.basicapplication.entitymappings.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.model.Project;
import com.basicapplication.entitymappings.service.ProjectService;

/**
 * @author Ram
 *
 */
@RestController
@RequestMapping("/project")
public class ProjectController {
	
	@Autowired
	ProjectService projectService;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Project create(@RequestBody ProjectDTO ProjectDTO) {

	
		return projectService.create(ProjectDTO);
	}
	
	
	@GetMapping
	public @ResponseBody List<ProjectDTO> getAllProjects() {
		return projectService.getAllProjects();
		
		
	}
	
	@GetMapping("/{id}")
	public @ResponseBody ProjectDTO getProject(@PathVariable("id") int id) {
		
		return projectService.getAProject(id);
	}
	
	
	@PutMapping(value="/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ProjectDTO updateProject(@RequestBody ProjectDTO ProjectDTO,@PathVariable("id") int id) {
		
		return projectService.updateProject(ProjectDTO,id);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteProject(@PathVariable("id") int id) {
		projectService.deleteProject(id);
	}


}
