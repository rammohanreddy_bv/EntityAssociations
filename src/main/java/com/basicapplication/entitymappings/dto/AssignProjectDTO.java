package com.basicapplication.entitymappings.dto;

import java.util.ArrayList;
import java.util.List;

import com.basicapplication.entitymappings.model.Project;

public class AssignProjectDTO {
	
	private int employeeId;
	
	private List<Integer> projects=new ArrayList<>();

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public List<Integer> getProjects() {
		return projects;
	}

	public void setProjects(List<Integer> projects) {
		this.projects = projects;
	}


}
