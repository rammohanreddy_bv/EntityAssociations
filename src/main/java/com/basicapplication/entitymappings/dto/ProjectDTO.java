/**
 * 
 */
package com.basicapplication.entitymappings.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



import com.basicapplication.entitymappings.model.Employee;
import com.basicapplication.entitymappings.model.Project;

/**
 * @author Ram
 *
 */
public class ProjectDTO {

	private String projectName;

	private String projectTechnology;

	private List<Integer> employeeIds = new ArrayList<>();
	
	public ProjectDTO() {
		
	}
	

	public ProjectDTO(String projectName, String projectTechnology, List<Integer> employeeIds) {
		super();
		this.projectName = projectName;
		this.projectTechnology = projectTechnology;
		this.employeeIds = employeeIds;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectTechnology() {
		return projectTechnology;
	}

	public void setProjectTechnology(String projectTechnology) {
		this.projectTechnology = projectTechnology;
	}

	public List<Integer> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(List<Integer> employeeIds) {
		this.employeeIds = employeeIds;
	}
	
	public static ProjectDTO getProjectDTO(Project project) {
		
		ProjectDTO dto=new ProjectDTO();
		dto.setProjectName(project.getProjectName());
		dto.setProjectTechnology(project.getProjectTechnology());
		Set<Employee> employees=new HashSet<>();
		List<Integer> ids=new ArrayList<>();
		for(Employee e:employees) {
			int id=e.getId();
			ids.add(id);
		}
		dto.setEmployeeIds(ids);
		return dto;
		
	}
	
	public Project toProject() {
		Project project = new Project();
		
		project.setProjectName(projectName);
		project.setProjectTechnology(projectTechnology);
		
		return project;
		
	}
	

}
