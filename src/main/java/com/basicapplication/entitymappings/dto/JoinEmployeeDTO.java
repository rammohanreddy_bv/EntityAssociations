/**
 * 
 */
package com.basicapplication.entitymappings.dto;

import java.util.ArrayList;
import java.util.List;

import com.basicapplication.entitymappings.model.Employee;

/**
 * @author Ram
 *
 */
public class JoinEmployeeDTO {
	
	private int projectId;
	
	private List<Integer> employees=new ArrayList<>();
	
	
	

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public List<Integer> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Integer> employees) {
		this.employees = employees;
	}

	
	
	
	

}
