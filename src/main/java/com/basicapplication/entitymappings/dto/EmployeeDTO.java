/**
 * 
 */
package com.basicapplication.entitymappings.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.basicapplication.entitymappings.model.Employee;
import com.basicapplication.entitymappings.model.Project;

/**
 * @author Ram
 *
 */
public class EmployeeDTO {
	
	private String employeeName;
	
	private String employeeCompany;
	
	private List<Integer> projectIds=new ArrayList<>();
	
	public EmployeeDTO() {
		
	}
	
	public EmployeeDTO(String employeeName, String employeeCompany, List<Integer> projectIds) {
		super();
		this.employeeName = employeeName;
		this.employeeCompany = employeeCompany;
		this.projectIds = projectIds;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeCompany() {
		return employeeCompany;
	}

	public void setEmployeeCompany(String employeeCompany) {
		this.employeeCompany = employeeCompany;
	}

	public List<Integer> getProjectIds() {
		return projectIds;
	}

	public void setProjectIds(List<Integer> projectIds) {
		this.projectIds = projectIds;
	}
	
	public static EmployeeDTO getEmployeeDTO(Employee employee) {
		EmployeeDTO dto=new EmployeeDTO();
		dto.setEmployeeCompany(employee.getEmployeeCompany());
		dto.setEmployeeName(employee.getEmployeeName());
		
		Set<Project> projects=employee.getProjectList();
		
		List<Integer> ids=new ArrayList<>();
		
		for(Project p:projects) {
			int id=p.getId();
			ids.add(id);
		}
		dto.setProjectIds(ids);
		return dto;
	}
	
	public Employee toEmployee() {
		Employee employee=new Employee();
		employee.setEmployeeCompany(employeeCompany);
		employee.setEmployeeName(employeeName);
		
		return employee;
	}

}
