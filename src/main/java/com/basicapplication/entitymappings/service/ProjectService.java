/**
 * 
 */
package com.basicapplication.entitymappings.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.dto.JoinEmployeeDTO;
import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.model.Project;

/**
 * @author Needa-Con
 *
 */

@Service
public interface ProjectService {
	
	Project create(ProjectDTO projectDTO);

	List<ProjectDTO> getAllProjects();

	ProjectDTO getAProject(int id);

	ProjectDTO updateProject(ProjectDTO projectDTO,int id);

	void deleteProject(int id);
	
	List<EmployeeDTO> joinEmployees(JoinEmployeeDTO joinEmployeeDTO);

}
