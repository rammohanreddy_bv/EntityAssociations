package com.basicapplication.entitymappings.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.dto.JoinEmployeeDTO;
import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.model.Employee;
import com.basicapplication.entitymappings.model.Project;
import com.basicapplication.entitymappings.repositories.EmployeeRepository;
import com.basicapplication.entitymappings.repositories.ProjectRepository;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Project create(ProjectDTO projectDTO) {
		Project project=projectDTO.toProject();
		return projectRepository.save(project);
	}

	@Override
	public List<ProjectDTO> getAllProjects() {
List<Project> allProjects=(List<Project>) projectRepository.findAll();
		
		List<ProjectDTO> projectDTOs=new ArrayList<>();
		
		for(Project p:allProjects) {
			projectDTOs.add(ProjectDTO.getProjectDTO(p));
		}
		return projectDTOs;
	}

	@Override
	public ProjectDTO getAProject(int id) {
		Project p=projectRepository.findOne(id);
		if(p!=null) {
			return ProjectDTO.getProjectDTO(p);
		}
		return null;
	}

	@Override
	public ProjectDTO updateProject(ProjectDTO projectDTO, int id) {
		
		Project p=projectRepository.findOne(id);
		
		if(p!=null) {
			if(projectDTO.getProjectName()!=null) {
				p.setProjectName(projectDTO.getProjectName());
			}
			if(projectDTO.getProjectTechnology()!=null) {
				p.setProjectTechnology(projectDTO.getProjectTechnology());
			}
			
			Project p1=projectRepository.save(p);
			return ProjectDTO.getProjectDTO(p1);
		}
		return null;
	}

	@Override
	public void deleteProject(int id) {
		projectRepository.delete(id);
		
	}

	@Override
	public List<EmployeeDTO> joinEmployees(JoinEmployeeDTO joinEmployeeDTO) {
		Project p=projectRepository.findOne(joinEmployeeDTO.getProjectId());
		if(p!=null) {
			Employee e;
			
			for(Integer employeeId:joinEmployeeDTO.getEmployees()) {
				e=employeeRepository.findOne(employeeId);
				
				if(e!=null) {
					p.getEmployeeList().add(e);
				}
				
			}
			projectRepository.save(p);
			return listOfEmployees(p);
		}
		return null;
	}
	
	private List<EmployeeDTO> listOfEmployees(Project p) {

		List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>();
		Project p1=projectRepository.findOne(p.getId());

		for (Employee e : p1.getEmployeeList()) {
			employees.add(EmployeeDTO.getEmployeeDTO(e));
		}
		return employees;
	}

}
