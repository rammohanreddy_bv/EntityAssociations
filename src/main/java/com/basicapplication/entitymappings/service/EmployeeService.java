/**
 * 
 */
package com.basicapplication.entitymappings.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.basicapplication.entitymappings.dto.AssignProjectDTO;
import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.model.Employee;

/**
 * @author Ram
 *
 */

@Service
public interface EmployeeService {
	
	Employee create(EmployeeDTO employeeDTO);

	List<EmployeeDTO> getAllEmployees();

	EmployeeDTO getEmployee(int id);

	EmployeeDTO updateEmployee(EmployeeDTO employeeDTO,int id);

	void deleteEmployee(int id);
	
	List<ProjectDTO> assignProjects(AssignProjectDTO assignProjectDTO);

	

}
