/**
 * 
 */
package com.basicapplication.entitymappings.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.basicapplication.entitymappings.dto.AssignProjectDTO;
import com.basicapplication.entitymappings.dto.EmployeeDTO;
import com.basicapplication.entitymappings.dto.ProjectDTO;
import com.basicapplication.entitymappings.model.Employee;
import com.basicapplication.entitymappings.model.Project;
import com.basicapplication.entitymappings.repositories.EmployeeRepository;
import com.basicapplication.entitymappings.repositories.ProjectRepository;

/**
 * @author Ram
 *
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Override
	public Employee create(EmployeeDTO employeeDTO) {
		Employee employee=employeeDTO.toEmployee();
		return employeeRepository.save(employee);
		
		
	}

	/* (non-Javadoc)
	 * @see com.basicapplication.entitymappings.service.EmployeeService#getAllEmployees()
	 */
	@Override
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> allEmployees=(List<Employee>) employeeRepository.findAll();
		
		List<EmployeeDTO> employeeDTOs=new ArrayList<>();
		
		for(Employee e:allEmployees) {
			employeeDTOs.add(EmployeeDTO.getEmployeeDTO(e));
		}
		return employeeDTOs;
	}

	/* (non-Javadoc)
	 * @see com.basicapplication.entitymappings.service.EmployeeService#getEmployee(int)
	 */
	@Override
	public EmployeeDTO getEmployee(int id) {
		Employee e=employeeRepository.findOne(id);
		if(e!=null) {
			return EmployeeDTO.getEmployeeDTO(e);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.basicapplication.entitymappings.service.EmployeeService#updateEmployee(com.basicapplication.entitymappings.dto.EmployeeDTO)
	 */
	@Override
	public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO,int id) {
		Employee e=employeeRepository.findOne(id);
		
		if(e!=null) {
			if(employeeDTO.getEmployeeCompany()!=null) {
				e.setEmployeeCompany(employeeDTO.getEmployeeCompany());
			}
			if(employeeDTO.getEmployeeName()!=null) {
				e.setEmployeeName(employeeDTO.getEmployeeName());
			}
			Employee e1=employeeRepository.save(e);
			return EmployeeDTO.getEmployeeDTO(e1);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.basicapplication.entitymappings.service.EmployeeService#deleteEmployee(int)
	 */
	@Override
	public void deleteEmployee(int id) {
		employeeRepository.delete(id);

	}

	@Override
	public List<ProjectDTO> assignProjects(AssignProjectDTO assignProjectDTO) {
		Employee e=employeeRepository.findOne(assignProjectDTO.getEmployeeId());
		
		if(e!=null) {
			Project p;
			
			for(Integer projectId:assignProjectDTO.getProjects()) {
				p=projectRepository.findOne(projectId);
				if(p!=null) {
					e.getProjectList().add(p);
				}
			}
			employeeRepository.save(e);
			
			return listOfTeams(e.getProjectList());
		}
		return null;
	}
	
	private List<ProjectDTO> listOfTeams(Set<Project> projectList) {
		List<ProjectDTO> projects = new ArrayList<ProjectDTO>();
		for (Project p : projectList) {
			projects.add(ProjectDTO.getProjectDTO(p));
		}
		return projects;
	}

}
